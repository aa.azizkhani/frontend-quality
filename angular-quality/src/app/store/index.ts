import { ActionReducerMap, createSelector, MetaReducer } from '@ngrx/store';
import { environment } from '../../environments/environment';
import * as fromCustomer from './customer.reducer';
import * as fromOrder from './order.reducer';

export interface State {
  customer: fromCustomer.Customer;
  orders: fromOrder.State;
}

export const reducers: ActionReducerMap<State> = {
  customer: fromCustomer.reducer,
  orders: fromOrder.reducer
};


export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];

export const getCustomerState = (state: State) => state.customer;
export const getOrderState = (state: State) => state.orders;

export const getOrder = createSelector(getOrderState, state => state.items);
export const hasCustomer = createSelector(getCustomerState, customer => !!customer.name && customer.name.length > 0 && !!customer.number);
export const isCustomerPremium = createSelector(getCustomerState, fromCustomer.isPremium);
