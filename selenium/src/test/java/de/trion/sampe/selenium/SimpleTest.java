package de.trion.sampe.selenium;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

import java.util.concurrent.TimeUnit;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class SimpleTest {
    private WebDriver driver;

    @BeforeClass
    public static void setupClass() {
        WebDriverManager
                .firefoxdriver()
                .setup();
    }

    @Before
    public void setupTest() {
        final FirefoxOptions options = new FirefoxOptions();
        //for CI environment
        options.setHeadless(true);
        driver = new FirefoxDriver(options);
    }

    @After
    public void teardown() {
        if (driver != null) {
            driver.quit();
        }
    }

    @Test
    public void simpleWebdriverTest() throws Exception
    {

        driver.get("http://localhost:4200");
//        driver.get("http://docker.for.mac.host.internal:4200/");  //  ng serve --disable-host-check
        TimeUnit.SECONDS.sleep(2);

        final WebElement name = driver.findElement(By.id("customerName"));
        name.sendKeys("Karsten Sitterberg");
        TimeUnit.SECONDS.sleep(1); //longer video
        final WebElement id = driver.findElement(By.id("customerId"));
        id.sendKeys("300000");
        TimeUnit.SECONDS.sleep(1); //longer video

        final WebElement button = driver.findElement(By.cssSelector("section form .btn.btn-primary"));
        button.click();

        final WebElement header = driver.findElement(By.cssSelector(".row h2.col-12"));

        assertThat(header.getText(), is("Premiumangebote"));
        TimeUnit.SECONDS.sleep(2);
    }
}
